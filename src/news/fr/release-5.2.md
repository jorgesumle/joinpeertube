---
id: release-5.2
title: La version 5.2 de PeerTube est sortie !
date: June 21, 2023
---

Cette version vient avec un petit défi technique que nous sommes fiers d'avoir relevé ! Cette nouvelle fonctionnalité ne sera pas aussi visible qu'un changement graphique mais elle permettra de rendre l'hébergement d'une plateforme PeerTube plus facile, plus résiliente et moins chère.

Voyons de quoi il en retourne :)

#### Ceci est une résolution : transcodage vidéo

Le transcodage vidéo est le fait de convertir un fichier vidéo en plusieurs formats pouvant être diffusés sur les différents appareils. En plus clair, cela récupère votre fichier (mp4, par exemple) et le transforme en plusieurs formats lisibles par les navigateurs web, applications, etc… Le transcodage se fait au moment de l'envoi d'une vidéo sur l'instance PeerTube, ainsi que pendant les directs ou au moment de l'édition avec le Studio.

En plus des formats, le transcodage permet d'avoir plusieurs qualités de vidéo (de la bouillie de pixels à la possibilité de voir une fourmi en zoomant dans un panorama). Ce sont les fameux "720p", "1080p", etc.

Le souci est que transcoder des vidéos 4k de chatons demande beaucoup de ressources en processeur (le "CPU") pour le serveur. Même un petit chaton. Et qui dit gros CPU dit gros prix. C'est quand même dommage de devoir louer un serveur plus performant pour transcoder plus rapidement une grosse vidéo par mois.

Si seulement nous avions une solution à proposer… 🤔

![Sepia with a jetpack and a toolbox](/img/news/release-5.2/en/2020-05-21_Peertube-Plugin_by-David-Revoy%20lowres_min500.jpg)

#### Ceci est une évolution : transcodage distant

La solution que nous vous proposons : le **transcodage distant** ! L'idée est de laisser les serveurs des plateformes PeerTube diffuser les vidéos, en rendant possible de déporter le travail de transcodage sur d'autres ordinateurs (par exemple des serveurs distants dédiés à la tâche, mutualisables, etc.). Vous trouverez comment faire dans [la documentation](https://docs.joinpeertube.org/admin/remote-runners).

On vous explique comment créer un de ces serveurs distants en y installant un « runner PeerTube » [ici](https://docs.joinpeertube.org/maintain/tools#peertube-runner). Il est même possible de transformer votre ordinateur personnel en runner grâce aux lignes de commandes, le temps de transcoder vos vidéos ! Si ça c'est pas de la souplesse…

Pour les plus techniques d'entre vous, cette fonctionnalité s'est inspirée des runners Gitlab pour l'architecture et de BOINC pour la simplicité d'utilisation. C'est une des forces du Libre : plutôt que de réinventer la roue, on s'inspire du travail des précurseurs et construit une nouvelle brique pour les personnes qui arriveront ensuite  !

Pour en savoir plus, rendez-vous sur [notre documentation](https://docs.joinpeertube.org/contribute/architecture#remote-vod-live-transcoding).

Vous pouvez voir concrètement comme faire sur [cette vidéo](https://peertube2.cpy.re/w/oJwHHYwt4oKjKhLNh2diAY) mais aussi en apprendre plus via cette vidéo de Jeena (qui partage ses chroniques d'administrateur de plateforme PeerTube sur son PeerTube, bien sûr) présentant le fonctionnement.

<div style="max-width: 560px; max-height: 315px; margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe title="PeerTube Admin 26: Distributed (Remote) Transcoding in 5.2.0" width="100%" height="100%" src="https://tube.jeena.net/videos/embed/dc9b7f39-3b99-435d-b49d-a8ec63acf3c3" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;"></iframe>
   </div>
</div>

#### Ceci est une (French) révolution

Avec la fédération des catalogues de vidéos, la diffusion en pair à pair en cas de succès d'une vidéo, la redondance des vidéos pour les plateformes PeerTube qui souhaitent s'entraider, ou encore la possibilité de stocker les vidéo sur un serveur spécialisé... PeerTube visait déjà à démocratiser l'hébergement et la diffusion de vidéos en ligne.

Le transcodage distant permet d'aller plus loin en encourageant la mutualisation des tâches gourmandes en ressources CPU, et (nous l'espérons) en incitant les communautés à travailler ensemble. Désormais, héberger une plateforme PeerTube peut se faire sur des serveurs très peu puissants (et donc pas trop chers).

De plus, cette nouvelle fonctionnalité a été développée pour permettre, à l'avenir, de prendre en charge d'autres tâches qui demandent de la ressource CPU, afin  de pourvoir les exécuter à distance elles aussi (comme par exemple les transcriptions vidéo audio-to-text).

Une fonctionnalité technique telle que le transcodage distant n'est pas des plus sexy. Ces développements ne sont pas les plus simples à financer, surtout par les dons. Nous remercions [NLnet](https://nlnet.nl/) de nous avoir accompagné·es et donné accès aux programmes de financements de NGI0, qui ont financé ce développement !

#### Ce sont des améliorations

En plus du transcodage distant, cette 5.2 présentes de nombreuses améliorations :

   * [Flux RSS pour podcast](https://github.com/Chocobozzz/PeerTube/pull/5487) : une première brique a été développée pour permettre l'intégration de vos contenus PeerTube dans des lecteurs de podcast !
   * Navigation sans souris : si vous aimez naviguer uniquement à l'aide du clavier, vous allez apprécier les améliorations apportées !
   * La fonctionnalité Studio (introduite dans la version 4.2) permettant de modifier une vidéo téléversée, a enfin [sa documentation](https://docs.joinpeertube.org/fr/use/studio) (même si c'est d'une simplicité déconcertante) !
   * [Définir la visibilité d'un replay](https://github.com/Chocobozzz/PeerTube/pull/5692) : il est désormais possible de définir une visibilité pour le replay différente de celle du live. Vous pouvez donc lancer un live en public mais mettre le replay en limité (ou inversement, ou autre) ! Cette fonctionnalité a été développée par Wicklow, stagiaire sur le développement PeerTube : merci à lui !
   ![image replay privacy](/img/news/release-5.2/fr/fr_replay_privacy.png)


#### Ceci est un appel aux dons

Le transcodage distant est tout nouveau : nous avons besoin de vos retours pour l'améliorer (mais vous pouvez aussi juste nous dire que vous appréciez). Pour cela, le meilleur endroit est [le forum (en français)](https://framacolibri.org/c/peertube/peertube/37) (ou [celui en anglais](https://framacolibri.org/c/peertube/38)).

**La suite ?!** En route vers la v6, vers la fin de l'année. Dans cette version vous pourrez, entre autres, protéger des vidéos par une mot de passe (grâce à Wicklow !), avoir un aperçu de l'image en survolant la barre de lecture, ajouter des chapitres à votre vidéo voire téléverser une nouvelle version de votre vidéo. Bref : nous avons du pain sur la planche !

Vous voulez nous aider à améliorer PeerTube ? Vous le pouvez **en contribuant au logiciel**, **en partageant cette information** et (si vous pouvez vous le permettre) en faisant [un don à Framasoft](https://support.joinpeertube.org/fr/).

Merci d'avance de votre soutien !

Framasoft
