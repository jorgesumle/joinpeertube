---
id: release-5.1
title: PeerTube 5.1 is out!
date: March 28, 2023
---

Version 5.1 of PeerTube has been released! On the menu: moderation of account creation, a button to resume live broadcasting, improved management of external authentication plugins... and other useful developments. Let's take a tour of the new features!

#### Moderated account creation requests

First new feature in this minor release: account creation requests [can now be validated a priori](https://docs.joinpeertube.org/admin/managing-users#registration-approval) by administrators.

When this feature is enabled and a person registers on an instance, they will have to fill in a field (such as "*Why do I want to create an account on this platform?*") and then wait for their registration to be validated before they can access their account.

![](/img/news/release-5.1/en/registration-reason.png)


The moderators see the different requests and can accept or reject them.

![](/img/news/release-5.1/en/registration-list.png)

An email is sent directly to the user when the request is processed. The account is automatically created when the request is approved.

![](/img/news/release-5.1/en/registration-accept.png)

In this way, we hope to allow different instances to reopen their registrations without risking potential waves of spam.

#### New "Resume Live" button

A "resume live" button has been added to the player! The button is red when the player is synchronized with the current live stream and grey when it is not. **A single click to resynchronise the live stream** is quite handy!

![](/img/news/release-5.1/en/screenshot-bouton-live.jpg)

#### Improved management of external authentication plugins

Developers will be able to take advantage of an **improvement to the API [for external authentication plugins](https://github.com/Chocobozzz/PeerTube/blob/develop/CHANGELOG.md#pluginsthemesembed-api)**: define a quota for users, update users or implement an automatic redirect to the external service when a session expires. These improvements were financed by the Department of Public Education (DIP) of the State of Geneva. Thanks!

#### And more...

This version has seen various improvements to the accessibility of the interface, as well as performance improvements (optimized rendering of the home page editor and more efficient comment retrieval).

Another development worth mentioning is the arrival of two new languages: Icelandic and Ukrainian. Thanks to the contributors for these translations!

We have also fixed many bugs reported by the community. We are now up to [more than 4000 tickets processed](https://github.com/Chocobozzz/PeerTube/issues) since the beginning of the PeerTube project, and that seems huge! This time spent improving, maintaining and supporting the software is **funded directly by you**, [through your donations](https://support.joinpeertube.org/). Thank you very much!

We hope you find this new version useful and we thank again all PeerTube contributors!

Framasoft
