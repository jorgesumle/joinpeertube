---
id: release-5.2
title: Version 5.2 of PeerTube is out!
date: June 21, 2023
---

This version comes with a small technical challenge that we're proud to have overcome! This new feature won't be as visible as a graphical change, but it will make hosting a PeerTube platform easier, more resilient and cheaper.

Let's see what it's all about :)

#### This is a resolution: video transcoding

Video transcoding is the process of converting a video file into different formats that can be played on different devices. In simple terms, it takes your video file (for example, mp4) and converts it into different formats that can be read by web browsers, applications, etc. Transcoding is done when a video is uploaded to the PeerTube instance, during live broadcasts or when editing your video with the PeerTube Studio.

In addition to the formats, transcoding allows you to have different video qualities (from pixel mush to the possibility of seeing an ant when zooming in on a panorama). These are the famous '720p', '1080p' and so on.

The problem is that transcoding 4k videos of kittens requires a lot of server computing resources (called "CPU"). Even a small kitten. And big CPUs mean big prices. It's a shame to have to rent a more powerful server just to transcode faster one big video a month.

If only we had a solution... 🤔

![Sepia with a jetpack and a toolbox](/img/news/release-5.2/en/2020-05-21_Peertube-Plugin_by-David-Revoy%20lowres_min500.jpg)

#### This is an evolution: remote transcoding

The solution we propose: **remote transcoding**! The idea is to let the servers of the PeerTube platform broadcast the videos, by having the ability to run transcoding tasks on other computers (for example, remote servers, that could be dedicated to the task, that could be shared, etc.). You can find out how to do this in the [documentation](https://docs.joinpeertube.org/admin/remote-runners).

We explain [here how to set up one of these remote servers](https://docs.joinpeertube.org/maintain/tools#peertube-runner) by installing a "PeerTube runner" on it. You can even turn your personal computer into a runner using command lines, just long enough to transcode your videos! Yes, it is THAT flexible!

For the more tech-savvy among us, this feature was inspired by Gitlab runners for their architecture and BOINC for their ease of use. This is one of the strengths of free-libre softwares: rather than reinventing the wheel, we take inspiration from the work of those who were there before and build a new brick for those who come after us!

You can find out more in [our documentation](https://docs.joinpeertube.org/contribute/architecture#remote-vod-live-transcoding).

You can see how it's done on [this video](https://peertube2.cpy.re/w/oJwHHYwt4oKjKhLNh2diAY) and also learn more from this video of Jeena (who shares his chronicles as a PeerTube platform administrator on his PeerTube instance, of course) presenting how remote transcoding works.

<div style="max-width: 560px; max-height: 315px; margin: auto;">
   <div style="position: relative; padding-top: 56.25%">
      <iframe title="PeerTube Admin 26: Distributed (Remote) Transcoding in 5.2.0" width="100%" height="100%" src="https://tube.jeena.net/videos/embed/dc9b7f39-3b99-435d-b49d-a8ec63acf3c3" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" style="position: absolute; inset: 0px;"></iframe>
   </div>
</div>


#### This is a (French) revolution

With the federation of video catalogues, the peer-to-peer broadcasting in the event of a video's success, the redundancy of videos for PeerTube platforms that want to help each other, the possibility of storing videos on a dedicated server... PeerTube has always aimed to democratize online video hosting and distribution.

Remote transcoding takes a step further by encouraging the pooling of CPU-intensive tasks and (we hope) encouraging communities to work together. From now on, a PeerTube platform can be hosted on very low-power (and therefore low-cost) servers.

In addition, this new feature has been designed to, in the future, enable other CPU-intensive tasks to be performed remotely (such as audio-to-text video transcription).

A technical feature like remote transcoding isn't exactly sexy. These developments are not the easiest to fund, especially through donations. We'd like to thank [NLnet](https://nlnet.nl/) for supporting us and giving us access to NGI0's funding programs that funded this feature!

#### These are improvements

In addition to remote transcoding, 5.2 includes a number of improvements:

   * [RSS feeds for podcasts](https://github.com/Chocobozzz/PeerTube/pull/5487): a first building block has been developed to allow your PeerTube content to be integrated into podcast players!
   * Mouse-free navigation: if you like to navigate using only the keyboard, you'll love some improvements!
   * The Studio feature (introduced in version 4.2), which allows you to edit an uploaded video, has finally been [documented](https://docs.joinpeertube.org/use/studio) (even though it is incredibly simple)!
   * [Define the visibility of a replay](https://github.com/Chocobozzz/PeerTube/pull/5692): it is now possible to define a different visibility for a replay than for a live. This means you can make a live public, but make the replay private (or vice versa, or whatever)! This feature was developed by Wicklow, a PeerTube development intern: thanks to him!
   ![image replay privacy](/img/news/release-5.2/en/en_replay_privacy.png)


#### This is a call for donations

Remote transcoding is brand new: **we need your feedback** on how to improve it (and you can also just tell us you like it). The best place to do this is [our forum](https://framacolibri.org/c/peertube/38)).

**What's next?** PeerTube is on its way to v6, towards the end of the year. In this version you'll be able to password protect videos (thanks to Wicklow!), get a preview thumbnail by hovering over the playback bar, add chapters to your video and even upload a new version of your video. In short, we've got a lot of work to do!

Do you want to help us improve PeerTube? You can do so **by contributing to the software, sharing this information** and (if you can afford it) making a [donation to our not-for-proft, Framasoft](https://support.joinpeertube.org/).

Thanks in advance for your support!

Framasoft
