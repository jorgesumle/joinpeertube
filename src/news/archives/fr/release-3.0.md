---
id: release-3.0
title: PeerTube v3 est sorti, avec du direct en pair-à-pair !
date: January 7, 2021
---

<p>Bonjour,</p>
<p>Ces six derniers mois ont été très chargés, mais nous avons rempli notre feuille de
  route et nous venons de lancer la version 3 de PeerTube !</p>
<ul>
  <li>Lire tous les détails <a href="https://framablog.org/2021/01/07/peertube-v3-ca-part-en-live/"
      rel="noreferrer noopener" target="_blank" class="bottom-link">sur notre blog</a> ;</li>
  <li>Voir <a rel="noreferrer noopener" target="_blank"
      href="https://github.com/Chocobozzz/PeerTube/releases/tag/v3.0.0">la publication du code</a> par vous-mêmes ;</li>
  <li>Regardez notre making of <a rel="noreferrer noopener" target="_blank"
      href="https://framatube.org/videos/watch/8519184e-b0c0-45f9-a005-4baddcd41f88">Les Coulisses de PeerTube</a> (en
    français, sous-titres à venir).</li>
</ul>
<figure><img loading="lazy"
    src="https://framablog.org/wp-content/uploads/2021/01/2020-12-11_peertube-V3_by-David-Revoy-1024x466.jpeg" alt="">
  <figcaption>Illustration CC-BY david revoy</figcaption>
</figure>
<p>Nous tenons à remercier toutes les personnes qui ont contribué à la collecte finançant
  cette v3 pour leur générosité (surtout dans une période difficile pour toutes et tous). Merci également aux sponsors
  de cette v3, Octopuce, Code Lutin et le projet Debian. Enfin, nos remerciements les plus sincères à toutes les
  personnes qui, à leur manière, ont travaillé et contribué à la concrétisation de cette v3.</p>
<p><span>Nous vous souhaitons de joyeux directs libres et en pair-à-pair,</span><br> Framasoft. </p>
