---
id: crowdfunding-4
title: 'PeerTube crowdfunding newsletter #4'
date: 'October 16, 2018'
---

Hello everyone!

We are now in mid-October! As promised, we have just released the first stable version of PeerTube.

It implements all stretch goals we planned in our crowdfunding:

- Localization support (as we write these lines, PeerTube is already available in 13 different languages!)
- Subtitles support
- Ability to import videos through an URL (YouTube, Vimeo, Dailymotion and many others!)
- Ability to import a video through a torrent file or a magnet URI
- RSS feeds, allowing you to track new videos published in all federated PeerTube instances, in a specific PeerTube instance or in a video channel you like. You can also subscribe to comment feeds!
- A more relevant search, with the ability to set advanced filters (duration, category, tags...)
- Subscriptions throughout the federation: you can follow your favorite video channels and see all the videos on a dedicated page
- Redundancy system: a PeerTube instance can help sharing some videos from another instance

We know that feature descriptions are not very amusing, so we have published a few demonstration videos:

<ul>
  <li>
    <a target="_blank" rel="noopener noreferrer" href="https://framatube.org/videos/watch/f57da309-6b92-4fe0-9267-ff8188cc050c">RSS Feeds</a>
  </li>

  <li>
    <a target="_blank" rel="noopener noreferrer" href="https://framatube.org/videos/watch/dcad56d9-9fe6-45bc-96aa-3d778f6804c1">Torrent import</a>
  </li>

  <li>
    <a target="_blank" rel="noopener noreferrer" href="https://framatube.org/videos/watch/59d306c0-fc5b-493a-956a-43785693346b">YouTube video import</a>
  </li>

  <li>
    <a target="_blank" rel="noopener noreferrer" href="https://framatube.org/videos/watch/edd7a468-08d5-4877-b62b-61c5f3f83ceb">Adding subtitles</a>
  </li>

  <li>
    <a target="_blank" rel="noopener noreferrer" href="https://framatube.org/videos/watch/60c4bea4-6bb2-4fce-8d9f-8a522575419d">Advanced search</a>
  </li>

  <li>
    <a target="_blank" rel="noopener noreferrer" href="https://framatube.org/videos/watch/8968dbe1-a387-433b-a20f-37fe9f3ca8d5">Video channel subscriptions</a>
  </li>
</ul>

This is the last newsletter regarding the PeerTube crowdfunding. We would like to thank you one more time, for allowing us to greatly improve PeerTube, and therefore to promote a more decentralized web. But the journey does not end here: we will continue to work on the software, and there is still a lot to do to fully free up video streaming. But before anything, we'll take a few days off ;)

We remind you that you can ask questions on [the PeerTube forum](https://framacolibri.org/c/qualite/peertube). You can also contact us directly on https://contact.framasoft.org.

Cheers,
Framasoft
