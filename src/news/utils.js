import fm from 'front-matter'
import { readdirSync, readFileSync } from 'fs'
import { join, dirname } from 'path'
import { fileURLToPath } from 'url'
import { markdownIt } from './news-markdown-it.js'

const __dirname = dirname(fileURLToPath(import.meta.url))

function getFreshNewsArticles (language) {
  return requireArticlesFromDir(join(__dirname, language), false)
}

function getArchiveNewsArticles (language) {
  return requireArticlesFromDir(join(__dirname, 'archives', language), true)
}

export {
  getFreshNewsArticles,
  getArchiveNewsArticles
}

function requireArticlesFromDir (directory, isArchive) {
  return readdirSync(directory)
    .map(file => readFileSync(join(directory, file), 'utf8').toString())
    .map(content => fm(content))
    .map(data => {
      data.id = data.attributes.id
      data.title = data.attributes.title
      data.date = new Date(data.attributes.date)
      data.shortDescription = buildShortDescription(data.body)
      data.isArchive = isArchive
      data.image = data.attributes.image

      return data
    })
}

function buildShortDescription (body) {
  return markdownIt.render(body)
    .replace(/(<([^>]+)>)/ig, '')
    .replaceAll('\n', ' ')
    .replace(/\s+/g, ' ')
    .trim()
    .slice(0, 150) + '...'
}
