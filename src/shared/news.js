function buildArticles (markdownFilesObj) {
  return Object.keys(markdownFilesObj)
    .map(key => markdownFilesObj[key])
    .map(a => {
      return {
        id: a.attributes.id,
        title: a.attributes.title,
        mastodon: a.attributes.mastodon,
        twitter: a.attributes.twitter,
        date: new Date(a.attributes.date),
        html: a.html
      }
    })
    .sort((a, b) => {
      if (a.date < b.date) return 1
      if (a.date > b.date) return -1
      return 0
    })
}

function buildSpecificArticle (markdownFilesObj, articleId) {
  const articles = buildArticles(markdownFilesObj)
    .filter(f => f.id === articleId)

  return articles[0]
}

export {
  buildArticles,
  buildSpecificArticle
}
